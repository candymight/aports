# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kronometer
pkgver=2.2.3
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://apps.kde.org/kronometer"
pkgdesc="A simple chronometer application"
license="GPL-2.0-or-later AND CC0-1.0"
makedepends="$depends_dev
	extra-cmake-modules
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdoctools-dev
	ki18n-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/kronometer/$pkgver/src/kronometer-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
77f948ae3a03b15a0cd49bc1048897bff0788a4831beff8f336270d8af6ee1a245d2174f5a3adc224257e9f319958197af7900ce8389b312f8fe7cd6c53a71d1  kronometer-2.2.3.tar.xz
"
